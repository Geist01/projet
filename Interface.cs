﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Projet.BDD;
using Projet.BDD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static Projet.Crud;

namespace Projet
{
    class Interface
    {
        public void Crud(ProjetDBContext context)
        {

            bool stop = false;
            bool quit = false;
            Crud Crud = new Crud(context);


            while (!stop)
            {
                Console.WriteLine("1 - Consulter la liste de vos clients");
                Console.WriteLine("2 - Rechercher un client");
                Console.WriteLine("3 - Ajouter un clients");
                Console.WriteLine("4 - Modifier un client");
                Console.WriteLine("5 - Supprimer un client");
                Console.WriteLine("6 - Quitter\n");

                string key = Console.ReadKey(true).KeyChar.ToString();
                while (Console.ReadKey(true).Key != ConsoleKey.Enter) ;

            
                switch (key)
                {
                    //Liste clients
                    case "1":
                        List<Client> Clients = Crud.GetListClient();
                        int i = 1;
                        foreach(Client Cli in Clients)
                        {
                            Console.WriteLine(i + " - " + Cli.Identifiant + " : ");
                            Console.WriteLine("\t Nom : " + Cli.Nom);
                            Console.WriteLine("\t Prénom : " + Cli.Prenom);
                            Console.WriteLine("\t Mail : " + Cli.Mail);
                            i++;
                        }
                        break;
                    //Recherche d'un ou plusieurs clients
                    case "2":                       
                        Console.WriteLine("Saisissez l'identifiant, le nom ou le prenom du client recherché.");
                        string recherche = Console.ReadLine();
                        List<Client> ListeClients = Crud.GetClientFiltre(recherche);
                        switch (ListeClients.Count)
                        {
                            case 0:
                                Console.WriteLine("Aucun Client ne correspond à cette recherche");
                                break;
                            case 1:
                                Client client = ListeClients[0];
                                Console.WriteLine(client.Identifiant + " : ");
                                Console.WriteLine("\t Nom : " + client.Nom);
                                Console.WriteLine("\t Prénom : " + client.Prenom);
                                Console.WriteLine("\t Mail : " + client.Mail);
                                break;
                            default:
                                Console.WriteLine("Il existe plusieurs clients correspondant à cette recherche : ");
                                int j = 0;
                                foreach (Client Cli in ListeClients)
                                {
                                    Console.WriteLine(j + " - " + Cli.Identifiant + " : ");
                                    Console.WriteLine("\t Nom : " + Cli.Nom);
                                    Console.WriteLine("\t Prénom : " + Cli.Prenom);
                                    Console.WriteLine("\t Mail : " + Cli.Mail);
                                    j++;
                                }
                                break;
                        }                          
                        break;
                    //Ajout d'un client
                    case "3":
                        Client clientAjoute = new Client();
                        string identifiant;
                        bool IdOk = false;
                        do
                        {
                            Console.WriteLine("Veuillez saisir l'identifiant de votre client : ");
                            identifiant = Console.ReadLine();
                            if (!Crud.CheckDuplicate(identifiant))
                            {
                                Console.WriteLine("Cet identifiant est déjà utilisé \n");
                            }
                            else
                            {
                                IdOk = true;
                            }
                        }
                        while (!IdOk);                       
                        clientAjoute.Identifiant = identifiant;
                        Console.WriteLine("Veuillez saisir le nom de votre client : ");
                        string nom = Console.ReadLine();
                        clientAjoute.Nom = nom;
                        Console.WriteLine("Veuillez saisir le prenom de votre client : ");
                        string prenom = Console.ReadLine();
                        clientAjoute.Prenom = prenom;
                        Console.WriteLine("Veuillez saisir le mail de votre client : ");
                        string mail = Console.ReadLine();
                        clientAjoute.Mail = mail;                       
                        Console.WriteLine("Confirmez-vous l'ajout de ce client ? \n");
                        string keyAjout = Console.ReadKey(true).KeyChar.ToString();
                        while (Console.ReadKey(true).Key != ConsoleKey.Enter) ;
                        if(keyAjout.ToLower() == "o")
                        {
                            Resultat Resultat = Crud.AddClient(clientAjoute);
                            if(Resultat.result)
                            {
                                Console.WriteLine("Succès de l'ajout");
                            }
                            else
                            {
                                Console.WriteLine("Erreur de l'ajout : " + Resultat.message);
                            }
                        }
                        break;
                    //Cas modification d'un client
                    case "4":
                        bool IdExits = false;
                        string id = "";
                        do
                        {
                            Console.WriteLine("Veuillez saisir l'identifiant du client à modifier : ");
                            id = Console.ReadLine();
                            Console.WriteLine("");
                            if (Crud.GetClient(id) != null)
                            {
                                IdExits = true;
                            }
                            else
                            {
                                Console.WriteLine("\nIdentifiant inconnu\n");
                            }
                        }
                        while (!IdExits);
                        Client ClientModif = Crud.GetClient(id);

                        bool StopModif = false;
                        while (!StopModif)
                        {
                            Console.WriteLine(ClientModif.Identifiant + " : ");
                            Console.WriteLine("\t Nom : " + ClientModif.Nom);
                            Console.WriteLine("\t Prénom : " + ClientModif.Prenom);
                            Console.WriteLine("\t Mail : " + ClientModif.Mail + "\n");
                            Console.WriteLine("1 - Modifier Nom");
                            Console.WriteLine("2 - Modifier Prenom");
                            Console.WriteLine("3 - Modifier Mail");
                            Console.WriteLine("4 - Quitter modification \n");
                            string keyModif = Console.ReadKey(true).KeyChar.ToString();
                            while (Console.ReadKey(true).Key != ConsoleKey.Enter) ;
                        
                            switch (keyModif)
                            {
                                case "1":
                                    Console.Write("Nouveau nom : ");
                                    ClientModif.Nom = Console.ReadLine();
                                    Console.WriteLine(ClientModif.Nom + " ? Confirmez-vous ce changement ? O/N \n");
                                    string ConfirmationNom = Console.ReadKey(true).KeyChar.ToString();
                                    while (Console.ReadKey(true).Key != ConsoleKey.Enter) ;
                                    if (ConfirmationNom.ToLower() == "o")
                                    {
                                        if (Crud.UpdateClient(ClientModif).result)
                                        {
                                            Console.WriteLine("Succès du changement \n");
                                        }
                                        else
                                        {
                                            Console.WriteLine("Echec du changement \n");
                                        }
                                    }
                                    break;
                                case "2":
                                    Console.Write("Nouveau prenom : ");
                                    ClientModif.Prenom = Console.ReadLine();
                                    Console.WriteLine(ClientModif.Prenom + " ? Confirmez-vous ce changement ? O/N");
                                    string ConfirmationPrenom = Console.ReadKey(true).KeyChar.ToString();
                                    while (Console.ReadKey(true).Key != ConsoleKey.Enter) ;
                                    if (ConfirmationPrenom.ToLower() == "o")
                                    {
                                        if (Crud.UpdateClient(ClientModif).result)
                                        {
                                            Console.WriteLine("Succès du changement");
                                        }
                                        else
                                        {
                                            Console.WriteLine("Echec du changement");
                                        }
                                    }
                                    break;
                                case "3":
                                    Console.Write("Nouveau mail : ");
                                    ClientModif.Mail = Console.ReadLine();
                                    Console.WriteLine(ClientModif.Mail + " ? Confirmez-vous ce changement ? O/N");
                                    string ConfirmationMail = Console.ReadKey(true).KeyChar.ToString();
                                    while (Console.ReadKey(true).Key != ConsoleKey.Enter) ;
                                    if (ConfirmationMail.ToLower() == "o")
                                    {
                                        if (Crud.UpdateClient(ClientModif).result)
                                        {
                                            Console.WriteLine("Succès du changement");
                                        }
                                        else
                                        {
                                            Console.WriteLine("Echec du changement");
                                        }
                                    }
                                    break;
                                case "4":
                                    StopModif = true;
                                    break;
                                default:
                                    Console.WriteLine("Touche non reconnue");
                                    break;
                            }
                        }
                        break;
                    //Suppression d'un client
                    case "5":
                        Console.WriteLine("Veuillez saisir l'identifiant du client à supprimer : ");
                        string idSuppr = Console.ReadLine();
                        Console.WriteLine("Confirmez-vous la suppression ? o/n \n");
                        string ConfirmationSuppr = Console.ReadKey(true).KeyChar.ToString();
                        while (Console.ReadKey(true).Key != ConsoleKey.Enter) ;
                        if(ConfirmationSuppr.ToLower() == "o")
                        {
                            if (Crud.DeleteClient(idSuppr))
                            {
                                Console.WriteLine("Succès de la suppression");
                            }
                            else
                            {
                                Console.WriteLine("Echec de la suppression");
                            }
                        }
                        break;
                    case "6":
                        stop = true;
                        quit = true;
                        break;
                    default:
                        Console.WriteLine("Touche non reconnue");
                        break;
                }
                if (!quit)
                {
                    Console.WriteLine("\nSouhaitez-vous effectuer une autre action ? o/n \n");
                    string quitter = Console.ReadKey(true).KeyChar.ToString();
                    while (Console.ReadKey(true).Key != ConsoleKey.Enter) ;
                    if(quitter.ToLower() == "n")
                    {
                        stop = true;
                    }
                }
            }      
        }
    }
}
