﻿using Microsoft.Extensions.Caching.Memory;
using Projet.BDD;
using Projet.BDD.Models;
using Projet.Custom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace Projet
{
    class Crud : Controller
    {
        public struct Resultat
        {
            public bool result;
            public string id;
            public string message;
        }

        private readonly ProjetDBContext db;

        public Crud(ProjetDBContext context)
        {
            db = context;
        }

        //Lecture de la liste des clients
        public List<Client> GetListClient()
        {
            try
            {
                List<Client> ListClients = db.Clients.ToList<Client>();
                return ListClients;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                return null;
            }
        }

        //Lecture Client par ID
        public Client GetClient(String Id)
        {
            try
            {
                Client Client = db.Clients.Find(Id);
                return Client;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                return null;
            }
        }

        //Lecture du ou des clients Client
        public List<Client> GetClientFiltre(String recherche)
        {
            try 
            {
                string recherchelower = recherche.ToLower();
                List<Client> Clients = db.Clients.Where(Item => Item.Identifiant.ToLower() == recherche || Item.Nom.ToLower() == recherche || Item.Prenom.ToLower() == recherche).ToList();                
                return Clients;
            }
            catch(Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                return null;
            }            
        }

        //Ajout du Client et retour de son ID
        public Resultat AddClient(Client Client)
        {
            Resultat Resultat = new Resultat();
            if (!Validation.valider(Client))
            {
                Resultat.result = false;
                Resultat.id = "";
                Resultat.message = "Modèle non conforme";
                return Resultat;
            }
            try
            {
                Client ClientAjoute = db.Clients.Add(Client).Entity;
                db.SaveChanges();
                Resultat.id = ClientAjoute.Identifiant;
                Resultat.result = true;
                Resultat.message = "";
                return Resultat;
            }
            catch(Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                Resultat.result = false;
                Resultat.id = "";
                Resultat.message = e.ToString();
                return Resultat;
            }
            
        }

        //Modification
        public Resultat UpdateClient(Client Client)
        {
            Resultat Resultat = new Resultat();
            if (!Validation.valider(Client))
            {
                Resultat.result = false;
                Resultat.id = "";
                Resultat.message = "Modèle non conforme";
                return Resultat;
            }
            try
            {
                db.Clients.Update(Client);
                db.SaveChanges();
                Resultat.id = Client.Identifiant;
                Resultat.result = true;
                Resultat.message = "Succès";
                return Resultat;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                Resultat.id = Client.Identifiant;
                Resultat.result = false;
                Resultat.message = "Echec";
                return Resultat;
            }
        }

        //Suppression
        public bool DeleteClient(string Id)
        {
            try
            {
                db.Clients.Remove(GetClient(Id));
                db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                return false;
            }
        }

        //Check doublon id
        public bool CheckDuplicate (string Id)
        {
            Client client = db.Clients.Where(item => item.Identifiant.ToLower().Trim() == Id.ToLower().Trim()).FirstOrDefault();
            if(client != null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
