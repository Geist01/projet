﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net.Mail;
using System.Text;

namespace Projet.Custom
{
    public class CheckEmailAttribute : ValidationAttribute
    {


        public string GetErrorMessage() => "Mail Invalide";

        protected override ValidationResult IsValid(object value,
            ValidationContext validationContext)
        {
            try
            {
                MailAddress m = new MailAddress(value as string);
                return ValidationResult.Success;
            }
            catch (FormatException f)
            {
                return new ValidationResult(GetErrorMessage() + " " + f.ToString());
            }
        }
    }
}
