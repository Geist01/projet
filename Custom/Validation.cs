﻿using Projet.BDD.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Projet.Custom
{
    class Validation
    {
        public static bool valider(Client Client)
        {
            ValidationContext context = new ValidationContext(Client, null, null);
            List<ValidationResult> validationResults = new List<ValidationResult>();
            bool valid = Validator.TryValidateObject(Client, context, validationResults, true);
            if (!valid)
            {
                foreach (ValidationResult validationResult in validationResults)
                {
                    Console.WriteLine("{0}", validationResult.ErrorMessage);
                }
                return false;
            }
            else
            {
                return true;
            }
            
        }
    }
}
