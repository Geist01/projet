﻿using CsvHelper;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Projet.BDD;
using Projet.BDD.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace Projet
{
    class Program
    {
        static void Main(string[] args)
        {
           
            var context = new ProjetDBContext();
           
            context.Database.Migrate();
            context.Clients.RemoveRange(context.Clients);


            Console.WriteLine("Veuillez saisir le chemin complet du dossier contenant le fichier client.txt : \n");

            Console.Write("Chemin : ");
            string filePath = Console.ReadLine() + "/client.txt";
            //string filePath = @"C:\Users\jeans\OneDrive\Documents\EPSI\I1\Utilisation des IDE\Lot 2\client.txt";

            // Vérifiez si le chemin existe ou non\?
            if (File.Exists(filePath))
            {

                Console.WriteLine("\nDébut du traitement...\n");

                int nberrors = 0;
                int nbclients = 0;
                TextReader reader = new StreamReader(filePath);
                var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture);
                csvReader.Configuration.HasHeaderRecord = false;
                csvReader.Configuration.Delimiter = ";";
                try
                {
                    List<Client> records = csvReader.GetRecords<Client>().ToList();
                    if(records.Count() == 0)
                    {
                        Console.WriteLine("\nLe fichier est vide\n");
                    }
                    else
                    {
                        foreach (Client record in records)
                        {
                            try
                            {
                                context.Clients.Add(record);                           
                                Console.WriteLine("Ligne ajoutée : Prénom : " + record.Prenom + "\nNom : " + record.Nom);
                                nbclients++;
                            }
                            catch (Exception e)
                            {
                                // handle the update exception
                                Console.WriteLine("\nUne erreur est survenue : " + e + "\n");
                                nberrors++;
                            }

                        }

                        if(nberrors == 0)
                        {
                            context.SaveChanges();
                        }

                        Console.WriteLine("\nFin du traitment, " + nberrors + " erreurs.\n");
                    } 
                }
                catch(Exception e)
                {
                    Console.WriteLine("\nUne erreur est survenue : " + e + "\n");
                }           
            }
            else
            {
                Console.WriteLine("\nLe fichier client.txt n'existe pas\n");
            }
            Console.WriteLine("\nVoulez-vous accéder à la gestion  de vos clients ? O/N\n");
            string key = Console.ReadKey(true).KeyChar.ToString();
            while (Console.ReadKey().Key != ConsoleKey.Enter) ;
            if(key.ToLower() == "o")
            {
                Interface Interface = new Interface();
                Interface.Crud(context);
            }
        }
    }
}
