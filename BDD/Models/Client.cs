﻿using Projet.Custom;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Projet.BDD.Models
{
    public class Client
    {
        [Key, Required]
        public string Identifiant { get; set; }
        [Required]
        public string Prenom { get; set; }
        [Required]
        public string Nom { get; set; }
        [Required]
        //[CheckEmail]
        [EmailAddress]
        public string Mail { get; set; }
  
    }
}
