﻿using Microsoft.EntityFrameworkCore;
using Projet.BDD.Models;



namespace Projet.BDD
{
    public class ProjetDBContext : DbContext
    {  
        public DbSet<Client> Clients { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\MSSQLLocalDB;Database=Projet;Trusted_Connection=True;");
        }

    }
}
